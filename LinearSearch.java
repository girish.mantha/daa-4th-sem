import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class LinearSearch {
    public static int linearSearch(int[] arr, int key) {
    	int count=0;
        for (int i = 0; i < arr.length; i++) {
        	count++;
            if (arr[i] == key) {
            	System.out.println("count: "+count);
                return i; // Key found, return the index
            }
        }
        System.out.println("count: "+count);
        return -1; // Key not found
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Read the key element from the user
        System.out.println("Enter the key element: ");
        int key = scanner.nextInt();

        // Read the input file
        System.out.println("Enter the path to the input file: ");
        String filePath = scanner.next();

        try {
            File file = new File(filePath);
            Scanner fileScanner = new Scanner(file);

            // Read the number of elements from the file
            int n = fileScanner.nextInt();

            // Read the elements into an array
            int[] numbers = new int[n];
            for (int i = 0; i < n; i++) {
                numbers[i] = fileScanner.nextInt();
            }

            fileScanner.close();

            long startTime = System.nanoTime();
            int result = linearSearch(numbers, key);
            long endTime = System.nanoTime();
            long timeTaken = (endTime - startTime);

            if (result != -1) {
                System.out.println("Key found at index " + result + " in " + n + " elements");
            } else {
                System.out.println("Key not found in " + n + " elements");
            }

            System.out.println("Time taken to search: " + timeTaken + " nanoseconds");
        } catch (FileNotFoundException e) {
            System.out.println("File not found: " + filePath);
        }

        scanner.close();
    }
}

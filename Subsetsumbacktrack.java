//Program 11 - Subset sum using backtracking technique

import java.util.Scanner;

class Subset
{
	int S[] = new int[10];
	int subset[] = new int[10];
	int d, n, k=0, flag=0, i=0, j;
	
	void subsetsum(int sum, int val)
	{
		sum = sum+val;
		if(val!=0)
			subset[k++] = val;
		
		if(sum==d)
		{
			System.out.print("{ ");
			for(j=0;j<k;j++)
				System.out.print(subset[j] + ", ");
			System.out.println("}");
			sum = sum-val;
			k--;
			flag=1;
			return;	
		}
		
		if(sum<d && i<n)
		{
			subsetsum(sum, S[i++]);
			subsetsum(sum, 0);
			i--;
		}
		else
		{
			k--;
		}
	}
}

public class Subsetsumbacktrack {
	public static void main(String args[])
	{
		int i;
		Scanner in = new Scanner(System.in);
		Subset s = new Subset();
		
		System.out.println("Enter the size of the set S");
		s.n = in.nextInt();
		
		System.out.println("\nEnter the elements of the set");
		for(i=0;i<s.n;i++)
			s.S[i] = in.nextInt();
		
		System.out.println("\nEnter the value of d");
		s.d = in.nextInt();
		
		System.out.println("\nSubset list");
		s.subsetsum(0, 0);
		
		if(s.flag==0)
			System.out.println("\nNo solution");
		in.close();				
	}
}

//Module-2 Knapsack problem using Greedy method

import java.util.Scanner;

class Knapsack
{
	int n, m;
	float profit[] = new float[10];
	float weight[] = new float[10];
	float pw[] = new float[10];
	float x[] = new float[10];
	
	int maxpw()
	{
		int i, max_i=0;
		float max=0;
		
		for(i=1;i<=n;i++)
			if(pw[i] > max)
			{
				max = pw[i];
				max_i = i;
			}	
		pw[max_i] = 0;
		return max_i;
	}
	
	void greedyknapsack()
	{
		int i=0, j=1;
		float U=m;
		
		while(j<=n)
		{
			i = maxpw();
			
			if(weight[i] <= U)
			{
				x[i] = 1;
				U = U - weight[i];
			}
			else
				break;			
			
			j++;
		}
		x[i] = (float)U/weight[i];
	}
}

public class KnapsackGreedy {
	public static void main(String args[])
	{
		int i; 
		float maxprofit=0, totalweight=0;
		
		Scanner in = new Scanner(System.in);
		Knapsack k = new Knapsack();
		
		System.out.println("Enter the number of items");
		k.n = in.nextInt();
		
		System.out.println("\nEnter the profits");
		for(i=1;i<=k.n;i++)
			k.profit[i] = in.nextInt();
		
		System.out.println("\nEnter the weights");
		for(i=1;i<=k.n;i++)
			k.weight[i] = in.nextInt();
		
		System.out.println("\nEnter the capacity");
		k.m = in.nextInt();
		
		for(i=1;i<=k.n;i++)
			k.pw[i] = k.profit[i]/k.weight[i];

		System.out.println("\nITEM\tPROFIT\tWEIGHT\tP/W");
		for(i=1;i<=k.n;i++)
			System.out.println(i+"\t"+k.profit[i]+"\t"+k.weight[i]+"\t"+k.pw[i]);
		
		k.greedyknapsack();
		
		for(i=1;i<=k.n;i++)
		{
			maxprofit += k.profit[i]*k.x[i];
			totalweight += k.weight[i]*k.x[i];
		}
								
		System.out.println("\nItems considered in the knapsack");
		for(i=1;i<=k.n;i++)
		{	if(k.x[i]!=0)
				if(k.x[i]==1)
					System.out.print("Item " + i + ",\t");
				else
					System.out.print(k.x[i] +" of Item " + i +",\t");
		}
		
		System.out.println("\n\nOptimum profit of knapsack\t: " + maxprofit);
		System.out.println("\nTotal weight of knapsack\t: " + totalweight);
		in.close();
	}
}

import java.util.Scanner;

//Program 10b - Implement Travelling salesperson problem using Dynamic programming

class TSP
{
	int c[][] = new int[10][10];
	int n, l=0;
	int S[] = new int[10];
	int J[][] = new int[50][3];
	int start;

	int mintourcost(int i, int S[], int m)
	{
		int p, j, k, min, mindex;
		int T[] = new int[10];
		int res[] = new int[10];

		if(m==0)
			return c[i][start];

		for(j=1;j<=m;j++)
		{
			p=1;
			for(k=1;k<=m;k++)
			{
				if(j!=k)
					T[p++] = S[k];
			}
			res[j] = c[i][S[j]] + mintourcost(S[j], T, m-1);
		}

		min = res[1];
		mindex = 1;

		for(j=2;j<=m;j++)
			if(res[j]<min)
			{
				min = res[j];
				mindex = j;
			}

		J[l][0] = i;
		J[l][1] = S[mindex];
		J[l++][2] = m;
		return min;
	}

	void mintourpath()
	{
		int i, m, cur, next;
		int visited[] = new int[10];

		cur = J[l-1][0];
		next = J[l-1][1];
		visited[cur] = visited[next] = 1;
		System.out.print(cur + "-->" + next);

		m = n-2;
		while(m>=1)
		{
			cur = next;
			for(i=0;i<l;i++)
			{
				if(J[i][0] == cur && J[i][2] == m)
				{
					next = J[i][1];
					if(visited[next]==0)
					{
						visited[next] = 1;
						System.out.print("-->" + next);
					}
				}
			}
			m--;
		}
		System.out.println("-->" + start);
	}
}

public class TravellingSalesPerson {
	public static void main(String args[])
	{
		int i, j, mincost;

		Scanner in = new Scanner(System.in);
		TSP t = new TSP();

		System.out.println("Enter the number of vertices");
		t.n = in.nextInt();

		System.out.println("\nEnter the cost matrix");
		for(i=1;i<=t.n;i++)
			for(j=1;j<=t.n;j++)
				t.c[i][j] = in.nextInt();

		System.out.println("\nEnter the starting vertex");
		t.start = in.nextInt();

		j=1;
		for(i=1;i<=t.n;i++)
			if(i!=t.start)
				t.S[j++] = i;

		mincost = t.mintourcost(t.start, t.S, t.n-1);
		System.out.println("\nCost of optimal tour is " + mincost);

		System.out.println("\nOptimal tour path");
		t.mintourpath();
		in.close();
	}
}

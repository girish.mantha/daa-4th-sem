//Kruskal algorithm

import java.util.Scanner;

class Kruskal
{
	int n;
	int c[][] = new int[10][10];
	int edges[][] = new int[30][3];
	int E[][] = new int[10][3];
	int parent[] = new int[10];
	int edgecount;
	
	void readedges()
	{
		int k=0;
		
		for(int i=0;i<n;i++)
			for(int j=i+1;j<n;j++)
				if(c[i][j]!=0 && c[i][j]!=99)
				{
					edges[k][0] = i;
					edges[k][1] = j;
					edges[k++][2] = c[i][j];
				}
		
		edgecount = k;
	}
	
	void sortedges()
	{
		int i, j, key, m, n;
		
		for(i=1;i<edgecount;i++)
		{
			m = edges[i][0];
			n = edges[i][1];
			key = edges[i][2];
			
			j = i-1;
			while(j>=0 && edges[j][2] > key)
			{
				edges[j+1][0] = edges[j][0];
				edges[j+1][1] = edges[j][1];
				edges[j+1][2] = edges[j][2];
				j = j-1;
			}
			
			edges[j+1][0] = m;
			edges[j+1][1] = n;
			edges[j+1][2] = key;
		}
	}
	
	int root(int i)
	{
		while(parent[i]!=-1)
			i = parent[i];
		return i;
	}
	
	boolean acyclic(int k)
	{
		int v, u, p, q;
		
		v = edges[k][0];
		u = edges[k][1];
		
		p = root(v);
		q = root(u);
		
		if(p!=q)
		{
			parent[q] = p;
			return true;
		}
		return false;		
	}
	
	void minspantree()
	{
		int ecounter=0, k=0, mincost=0;
		
		readedges();
		sortedges();
		
		for(int i=0;i<n;i++)
			parent[i] = -1;
		
		while(ecounter<n-1)
		{
			if(acyclic(k))
			{
				E[ecounter][0] = edges[k][0];
				E[ecounter][1] = edges[k][1];
				E[ecounter][2] = edges[k][2];
				mincost += edges[k][2];
				ecounter++;
			}
			k++;
		}
		
		System.out.println("\nEdges of the minimum spanning tree");
		for(int i=0;i<ecounter;i++)
			System.out.println("Edge " + E[i][0] + " " + E[i][1] + " at a cost of " + E[i][2]);
		
		System.out.println("\nMinimum cost: " + mincost);
	}
}

public class GreedyKruskals {
	public static void main(String args[])
	{
		Scanner in = new Scanner(System.in);
		Kruskal k = new Kruskal();
		
		System.out.println("Enter the number of vertices");
		k.n = in.nextInt();
		
		System.out.println("Enter the cost matrix");
		for(int i=0;i<k.n;i++)
			for(int j=0;j<k.n;j++)
				k.c[i][j] = in.nextInt();
		
		k.minspantree();
		in.close();
	}
}

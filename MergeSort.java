import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class MergeSort {
    public static void main(String[] args) {
    	
        int[] arr = readArrayFromFile( );
        
        System.out.println("Original array:");
        printArray(arr);
        
        long startTime = System.nanoTime();
        mergeSort(arr);
        long endTime = System.nanoTime();
        long duration = (endTime - startTime)/1000000;// Time in nanoseconds
        
        System.out.println("Sorted array:");
        printArray(arr);
        System.out.println("N: " + arr.length);
        System.out.println("Time taken: " + duration + " nanoseconds");
        
        writeArrayToFile(arr, "output.txt");
    }

    private static void mergeSort(int[] arr) {
        if (arr.length < 2) {
            return;
        }
        
        int mid = arr.length / 2;
        int[] left = Arrays.copyOfRange(arr, 0, mid);
        int[] right = Arrays.copyOfRange(arr, mid, arr.length);
        
        mergeSort(left);
        mergeSort(right);
        merge(arr, left, right);
    }
    
    private static void merge(int[] arr, int[] left, int[] right) {
        int i = 0, j = 0, k = 0;
        //int count=0;
        
        while (i < left.length && j < right.length) {
        	//count++;
            if (left[i] <= right[j]) {
                arr[k++] = left[i++];
            } else {
                arr[k++] = right[j++];
            }
        }
        
        while (i < left.length) {
            arr[k++] = left[i++];
        }
        
        while (j < right.length) {
            arr[k++] = right[j++];
        }
        //System.out.println("count = "+count);
    }

    public static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static int[] readArrayFromFile( ) {
        // Read the input file
    	Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the path to the input file: ");
        String filePath = scanner.next();
        File file = new File(filePath);
        Scanner fileScanner = null;
		try {
			fileScanner = new Scanner(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        // Read the number of elements from the file
        int n = fileScanner.nextInt();

        // Read the elements into an array
        int[] numbers = new int[n];
        for (int i = 0; i < n; i++) {
            numbers[i] = fileScanner.nextInt();
        }

        fileScanner.close();
        scanner.close();
        return numbers;
    }

    public static void writeArrayToFile(int[] arr, String filename) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
        		writer.write(arr.length + "\n");
            for (int num : arr) {
                writer.write(num + "\n");
            }
            writer.newLine(); // Add a new line at the end
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void printArray(int[] arr) {
        for (int num : arr) {
            System.out.print(num + " ");
        }
        System.out.println();
    }
}

//Finding single shortest path for a given vertex using Dijkstra's algorithm

import java.util.Scanner;

class Dijkstras
{
	int n;
	int a[][] = new int[10][10];
	int dist[] = new int[10];
	int penultimate[] = new int[10];
	int visited[] = new int[10];
	
	void qinsert(int v, int p, int d)
	{
		dist[v] = d;
		penultimate[v] = p;
	}
	
	int qmindelete()
	{
		int i, min, mindex=0;
		min = 99;
		for(i=0;i<n;i++)
		{
			if(dist[i] < min && visited[i]==0)
			{
				min = dist[i];
				mindex = i;
			}
		}
		return mindex;
	}
	
	void shortestpath(int source)
	{
		int i, j=0, k, ustar, u, du, pu;
		int V[][] = new int[10][3];
		
		for(i=0;i<n;i++)
			qinsert(i, -1, 99);
		
		dist[source] = 0;
		
		for(i=0;i<n;i++)
		{
			ustar = qmindelete();
			V[j][0] = ustar;
			V[j][1] = penultimate[ustar];
			V[j++][2] = dist[ustar];
			visited[ustar] = 1;
			
			for(k=0;k<n;k++)
			{
				if(a[ustar][k] !=0 && a[ustar][k]!=99 && visited[k]==0)
				{
					u = k;
					if(dist[ustar] + a[ustar][u] < dist[u])
					{
						du = dist[ustar] + a[ustar][u];
						pu = ustar;
						qinsert(u, pu, du);
					}
				}
			}		
		}
		
		System.out.println("Shortest path from vertex " + source + " to all other vertices");
		for(i=1;i<n;i++)
			System.out.println("To vertex " + V[i][0] + " via vertex " + V[i][1] + " at distance " + V[i][2]);	
	}
}

public class GreedyDijkras {
	public static void main(String args[])
	{
		int source;
		Dijkstras d = new Dijkstras();
		Scanner in = new Scanner(System.in);
		
		System.out.println("Enter the number of vertices: ");
		d.n = in.nextInt();
		
		System.out.println("Enter the cost matrix");
		for(int i=0;i<d.n;i++)
			for(int j=0;j<d.n;j++)
				d.a[i][j] = in.nextInt();
		
		System.out.println("Enter the starting vertex: ");
		source = in.nextInt();
				
		d.shortestpath(source);
		in.close();
		
	}
}

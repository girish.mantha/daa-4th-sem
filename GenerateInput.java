import java.util.*;
import java.io.*;

class GenerateInput {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        try {
            FileWriter fileWriter = new FileWriter("input.txt");
            System.out.print("Enter the number of elements: ");
            int n = sc.nextInt();
            Random rand = new Random();
            fileWriter.write(n + "\n");
            for (int i = 0; i < n; i++)
                fileWriter.write(rand.nextInt(10) + " ");
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Error: Unable to open output file!");
            System.exit(1);
        }

        sc.close();
    }
}

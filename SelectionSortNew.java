import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class SelectionSortNew {
    public static void main(String[] args) {
        int[] arr = readArrayFromFile( );
        
        System.out.println("Original array:");
        printArray(arr);
        
        long startTime = System.nanoTime();
        selectionSort(arr);
        long endTime = System.nanoTime();
        long duration = (endTime - startTime)/1000000;// Time in nanoseconds
        
        System.out.println("Sorted array:");
        printArray(arr);
        System.out.println("N: " + arr.length);
        System.out.println("Time taken: " + duration + " nanoseconds");
        
        writeArrayToFile(arr, "output.txt");
    }

    public static void selectionSort(int[] arr) {
        int n = arr.length;
        //int count = 0;
        for (int i = 0; i < n - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < n; j++) {
            	//count++;
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }
            int temp = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = temp;
        }
        //System.out.println("Step Count " + count);
    }

    public static int[] readArrayFromFile( ) {
        // Read the input file
    	Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the path to the input file: ");
        String filePath = scanner.next();
        File file = new File(filePath);
        Scanner fileScanner = null;
		try {
			fileScanner = new Scanner(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        // Read the number of elements from the file
        int n = fileScanner.nextInt();

        // Read the elements into an array
        int[] numbers = new int[n];
        for (int i = 0; i < n; i++) {
            numbers[i] = fileScanner.nextInt();
        }

        fileScanner.close();
        scanner.close();
        return numbers;
    }

    public static void writeArrayToFile(int[] arr, String filename) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
        		writer.write(arr.length + "\n");
            for (int num : arr) {
                writer.write(num + "\n");
            }
            writer.newLine(); // Add a new line at the end
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void printArray(int[] arr) {
        for (int num : arr) {
            System.out.print(num + " ");
        }
        System.out.println();
    }
}

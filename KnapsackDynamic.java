//Program 6a - Knapsack using Dynamic programming

import java.util.Scanner;

class Dknapsack
{
	int n, W;
	int values[] = new int[10];
	int weights[] = new int[10];
	int V[][] = new int[10][30];
	
	int max(int m, int n)
	{
		return m>n?m:n;
	}
	
	int dynamicknapsack()
	{
		int i, j;
		
		for(i=1;i<=n;i++)
		{
			for(j=1;j<=W;j++)
			{
				if(j < weights[i])
					V[i][j] = V[i-1][j];
				else
					V[i][j] = max(V[i-1][j], values[i] + V[i-1][j-weights[i]]);		
			}
		}
		return V[n][W];
	}
	
	void backtrackitems()
	{
		int i, j, k=0;
		int items[] = new int[10];
		
		i=n;
		j=W;
		
		while(i>0)
		{
			if(V[i][j] == V[i-1][j])
				i = i-1;
			else
			{
				items[k++] = i;
				j = j - weights[i];
				i = i - 1;
			}
		}
		
		for(i=k-1;i>=0;i--)
			System.out.print("\t" + items[i]);
	}
}

public class KnapsackDynamic {
	public static void main(String args[])
	{
		int i, j, res;
		Scanner in = new Scanner(System.in);
		Dknapsack k = new Dknapsack();
		
		System.out.println("Enter the no. of items");
		k.n = in.nextInt();
		
		System.out.println("\nEnter the weights");
		for(i=1;i<=k.n;i++)
			k.weights[i] = in.nextInt();
		
		System.out.println("\nEnter the values");
		for(i=1;i<=k.n;i++)
			k.values[i] = in.nextInt();
		
		System.out.println("\nEnter the capacity of knapsack");
		k.W = in.nextInt();
		
		res = k.dynamicknapsack();
				
		System.out.println("\nContents of the table");
		for(i=1;i<=k.n;i++)
		{
			for(j=1;j<=k.W;j++)
				System.out.print("\t" + k.V[i][j]);
			System.out.println();
		}
				
		System.out.println("\nOptimal value of knapsack: " + res);
		System.out.println("\nItems considered in the knapsack");
		k.backtrackitems();
		in.close();		
	}
}

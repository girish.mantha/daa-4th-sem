// Module-2 - Finding minimum spanning tree using Prim's algorithm

import java.util.Scanner;

class Prim
{
	int n;
	int c[][] = new int[10][10];
	int V[] = new int[10];
	int E[][] = new int[10][3];
	
	void minspantree(int source)
	{
		int i, j, k, min, v=0, u=0, mincost=0;
		
		V[source] = 1;
		for(k=0;k<n-1;k++)
		{
			min = 99;
			for(i=0;i<n;i++)
			{
				if(V[i] == 1)
				{
					for(j=0;j<n;j++)
					{
						if(c[i][j]!=0 && V[j]==0 && c[i][j]<min)
						{
							min = c[i][j];
							v = i; 
							u = j; 
						}
					}
				}
			}
			E[k][0] = v;
			E[k][1] = u;
			E[k][2] = c[v][u];
			mincost += c[v][u];
			V[u] = 1;			
		}
		System.out.println("Minimum spanning tree edges");
		for(i=0;i<n-1;i++)
			System.out.println("Edge " + E[i][0] + " " + E[i][1] + " at a cost " + E[i][2]);
		System.out.print("\nMinimum cost: " + mincost);
	}
	
}

public class GreedyPrims {
	public static void main(String args[])
	{
		int source;
		Scanner in = new Scanner(System.in);
		Prim p = new Prim();
		
		System.out.println("Enter the number of vertices");
		p.n = in.nextInt();
		
		System.out.println("Enter the cost matrix");
		for(int i=0;i<p.n;i++)
			for(int j=0;j<p.n;j++)
				p.c[i][j] = in.nextInt();
		
		System.out.println("Enter the source vertex");
		source = in.nextInt();
		
		p.minspantree(source);
		in.close();
	}
	
}

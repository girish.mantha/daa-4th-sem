//Floyd's algorithm for all pair shortest path algorithm

import java.util.Scanner;

class Floyd
{
	int n;
	int W[][] = new int[10][10];

	int min(int a, int b)
	{
		return a<b?a:b;
	}

	void shortestpath()
	{
		int i, j, k;
		for(k=1;k<=n;k++)
		{
			for(i=1;i<=n;i++)
				for(j=1;j<=n;j++)
					if(i!=k || j!=k)
						W[i][j] = min(W[i][j], W[i][k]+W[k][j]);
			//---
			System.out.println("\nDistance matrix");
			for(i=1;i<=n;i++)
			{
				for(j=1;j<=n;j++)
					System.out.print(W[i][j] + "\t");
				System.out.println();
			}
			//---
		}
	}

}

public class FloydsAlgorithm {
	public static void main(String args[])
	{
		int i, j;
		Scanner in = new Scanner(System.in);
		Floyd f = new Floyd();

		System.out.println("Enter the number of vertices: ");
		f.n = in.nextInt();

		System.out.println("\nEnter the weight matrix (99 for infinity)");
		for(i=1;i<=f.n;i++)
			for(j=1;j<=f.n;j++)
				f.W[i][j] = in.nextInt();

		f.shortestpath();

		System.out.println("\nDistance matrix");
		for(i=1;i<=f.n;i++)
		{
			for(j=1;j<=f.n;j++)
				System.out.print(f.W[i][j] + "\t");
			System.out.println();
		}

		in.close();
	}
}

//Module-5 - Hamiltonian cycle using backtracking method

import java.util.Scanner;

class Hamiltonian
{
	int G[][] = new int[10][10];
	int x[] = new int[10];
	int n, flag=0;
	
	void hamiltoniancycle(int k)
	{
		int i;
		while(true)
		{
			nextvalue(k);
			if(x[k]==0)
				return;
			if(k==n)
			{
				for(i=1;i<=n;i++)
					System.out.print(x[i]+"->");
				System.out.println(x[1]);
				flag=1;
			}
			else
				hamiltoniancycle(k+1);		
		}
	}
	
	void nextvalue(int k)
	{
		int j;
		while(true)
		{
			x[k] = (x[k]+1) % (n+1);
			if(x[k]==0)
				return;
			if(G[x[k-1]][x[k]] != 0)
			{
				for(j=1;j<=k-1;j++)
					if(x[j]==x[k])
						break;
				if(j==k)
					if(k<n || (k==n && G[x[n]][x[1]]!=0))
						return;
			}
		}
	}
}

public class HamiltonianBacktrack {
	public static void main(String args[])
	{
		int i, j, start;
		Scanner in = new Scanner(System.in);
		Hamiltonian h = new Hamiltonian();
		
		System.out.println("Enter the no. of vertices");
		h.n = in.nextInt();
		
		System.out.println("\nEnter the adjacency matrix");
		for(i=1;i<=h.n;i++)
			for(j=1;j<=h.n;j++)
				h.G[i][j] = in.nextInt();
		
		System.out.println("\nEnter the starting vertex");
		start = in.nextInt();
		
		System.out.println("\nHamiltonian cycles");
		h.x[1] = start;
		h.hamiltoniancycle(2);
		
		if(h.flag==0)
			System.out.println("No Hamiltonian cycles");
	}
}

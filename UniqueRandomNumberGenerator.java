import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class UniqueRandomNumberGenerator {
    public static void main(String[] args) {
        int totalNumbers = 5000; // Define the total number of unique random numbers you want to generate
        String fileName = "input.txt"; // Specify the file name and extension

        List<Integer> generatedNumbers = new ArrayList<>();
        Random random = new Random();

        while (generatedNumbers.size() < totalNumbers) {
            int randomNumber = random.nextInt(1000000);

            // Check if the generated number is unique
            if (!generatedNumbers.contains(randomNumber)) {
                generatedNumbers.add(randomNumber);
            }
        }

        // Write the generated unique random numbers to the file
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            writer.write(String.valueOf(totalNumbers)); // Write the total number of elements in the first line
            writer.newLine();

            for (int number : generatedNumbers) {
                writer.write(String.valueOf(number));
                writer.newLine();
            }
            System.out.println("Random numbers have been written to the file: " + fileName);
        } catch (IOException e) {
            System.out.println("An error occurred while writing to the file: " + e.getMessage());
        }
    }
}
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class QuickSort {
    public static void main(String[] args) {
    	int[] arr = readArrayFromFile( );
    
        
        System.out.println("Original array:");
        printArray(arr);
        
        long startTime = System.nanoTime();
        quickSort(arr, 0, arr.length - 1);
        long endTime = System.nanoTime();
        long duration = (endTime - startTime)/1000000;// Time in nanoseconds
        
        System.out.println("Sorted array:");
        printArray(arr);
        System.out.println("N: " + arr.length);
        System.out.println("Time taken: " + duration + " nanoseconds");
        
        writeArrayToFile(arr, "output.txt");
    }

    public static void quickSort(int[] arr, int low, int high) {
        if (low < high) {
            int pivotIndex = partition(arr, low, high);
            quickSort(arr, low, pivotIndex - 1);
            quickSort(arr, pivotIndex + 1, high);
        }
    }

    public static int partition(int[] arr, int low, int high) {
    	int i=low;
    	int pivot = arr[low];
    	//static int count=0;
    	low = low+1;
    	while(true) 
    	{
	    	while((pivot >= arr[low]))
	    		{low++;}//count++;)
	    	
	    	while((pivot < arr[high]) && (high >=0) )
	    		{high--;}//count++;}
	    	
	    	if(low<high)
	    		swap(arr,low,high);
	    	else
	    		break;
    	}
	    	//printf("\nStep Count = %d",count);
    	swap(arr,i,high);
    	return high;
    }

    public static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static int[] readArrayFromFile( ) {
        // Read the input file
    	Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the path to the input file: ");
        String filePath = scanner.next();
        File file = new File(filePath);
        Scanner fileScanner = null;
		try {
			fileScanner = new Scanner(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        // Read the number of elements from the file
        int n = fileScanner.nextInt();

        // Read the elements into an array
        int[] numbers = new int[n];
        for (int i = 0; i < n; i++) {
            numbers[i] = fileScanner.nextInt();
        }

        fileScanner.close();
        scanner.close();
        return numbers;
    }

    public static void writeArrayToFile(int[] arr, String filename) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
        		writer.write(arr.length + "\n");
            for (int num : arr) {
                writer.write(num + "\n");
            }
            writer.newLine(); // Add a new line at the end
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void printArray(int[] arr) {
        for (int num : arr) {
            System.out.print(num + " ");
        }
        System.out.println();
    }
}

public class BruteForceStringMatching {
    public static int searchString(String text, String pattern) {
        int n = text.length();
        int m = pattern.length();
        int c = 0;

        for (int i = 0; i <= n - m; i++) {
            int j;
            for (j = 0; j < m; j++) {
            	c++;
                if (text.charAt(i + j) != pattern.charAt(j))
                	break;
            }
            if (j == m)
            {
            	System.out.println("Step Count = " + c);
            	return i;
            }
        }
        System.out.println("Step Count = " + c);
        return -1;
    }

    public static void main(String[] args) {
        String text = "MONDAY MORNING CLASS";
        String pattern = "MOR";
        int index = searchString(text, pattern);
        if (index != -1) {
            System.out.println("Pattern found at index " + index);
        } else {
            System.out.println("Pattern not found in the text.");
        }
    }
}
